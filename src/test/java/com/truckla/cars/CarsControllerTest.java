package com.truckla.cars;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc
public class CarsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Disabled
    public void testOneCar() throws Exception {
        
        // TODO
        mockMvc.perform(get("/cars/1")).andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(content().json("{\"id\":1,\"manufacturer\":\"Ford\",\"model\":\"Model T\",\"build\":1927-01-01}"));

    }
}
